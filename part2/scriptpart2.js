'use strict';

// let hasDriversLicense = false;
// const passTest = true;

// if (passTest) hasDriversLicense = true;
// if (hasDriversLicense) console.log(`I can drive a car`);

// const interface = 'Audio';
// const private = 534;


// function logger() {
//     console.log('My name is Jonas');
// }

// logger();
// logger();
// logger();

// function fruitProcessor(apples, oranges) {
//     // console.log(apples, oranges);
//     const juice = `Jiuce with ${apples} apples and ${oranges} oranges.`;
//     return juice;
// }

// const appleJuice = fruitProcessor(5, 0);
// console.log(appleJuice);

// const appleOrangeJuice = fruitProcessor(2, 4);
// console.log(appleOrangeJuice);

// function calcaAge1(birthyear) {
//     return 2037 - birthyear;
// }

// const age1 = calcaAge1(1991);

// const calcaAge2 = function (birthyear) {
//     return 2037 - birthyear;
// }
// const age2 = calcaAge2(1991);
// console.log(age1, age2);

// arrow function
// const calcAge3 = birthYear => 2037 - birthYear;
// const age3 = calcAge3(1991);
// console.log(age3);

// const yearsUntilRetirement = (birthYear, firstName) => {
//     const age = 2037 - birthYear;
//     const retirement = 65 - age;
//     // return retirement
//     return `${firstName} retires in ${retirement} years`
// }

// console.log(yearsUntilRetirement(1991, 'Jonas'));
// console.log(yearsUntilRetirement(1980, 'Bob'));


// function cutFruitPieces(fruit) {
//     return fruit * 4;
// }

// function fruitProcessor(apples, oranges) {
//     const applePieces = cutFruitPieces(apples);
//     const orangesPieces = cutFruitPieces(apples);
//     const juice = `Jiuce with ${applePieces} pieces ofapples and ${orangesPieces} pieces of oranges.`;
//     return juice;
// }

// console.log(fruitProcessor(2, 3));

// const calcAge = function (birthYear) {
//     return 2037 - birthYear;
// }

// const hasRetirement = function (calcAge) {
//     const retirement = 65 - calcAge;
//     return retirement > 0 ? retirement : 0;
// }

// const yearsUntilRetirement = (birthYear, firstName) => {
//     return `${firstName} retires in ${hasRetirement(calcAge(birthYear))} years`;
// }

// console.log(yearsUntilRetirement(1991, 'Jonas'));
// console.log(yearsUntilRetirement(1970, 'Mike'));



// const calcAverge = (score1, score2, score3) => (score1 + score2 + score3) / 3;

// // const avgDolphins = calcAverge(44, 23, 71);
// // const avgCoalas = calcAverge(65, 54, 49);
// const avgDolphins = calcAverge(85, 54, 41);
// const avgCoalas = calcAverge(23, 34, 27);
// console.log(avgDolphins, avgCoalas);

// function checkWinner(avgDolphins, avgCoalas) {
//     if (avgDolphins >= 2 * avgCoalas) {
//         console.log(`Dolphinas wins (${avgDolphins} vs. ${avgCoalas})`);
//     } else if (avgCoalas >= 2 * avgDolphins) {
//         console.log(`Koalas wins (${avgCoalas} vs. ${avgDolphins})`);
//     } else {
//         console.log(`No teams wins...`);
//     }
// }

// const checkWinner = function (avgDolphins, avgCoalas) {
//     if (avgDolphins >= 2 * avgCoalas) {
//         console.log(`Dolphinas wins (${avgDolphins} vs. ${avgCoalas})`);
//     } else if (avgCoalas >= 2 * avgDolphins) {
//         console.log(`Koalas wins (${avgCoalas} vs. ${avgDolphins})`);
//     } else {
//         console.log(`No teams wins...`);
//     }
// }

// checkWinner(avgDolphins, avgCoalas);
// checkWinner(111, 345);

// const friends = ['Mike', 'Dan', 'Ben'];
// console.log(friends);
// const year = new Array(1985, 1967, 1999, 2008);
// console.log(year);
// console.log(friends[0]);
// console.log(friends[2]);
// console.log(friends.length);
// console.log(friends[friends.length - 1]);
// const jonas = ['Jonas', 'NAkdds', 2037 - 1991, 'teacher', friends];
// console.log(jonas);
// console.log(jonas.length);
// console.log(typeof jonas);


// const calcAge = function (birthYear) {
//     return 2037 - birthYear;
// }
// const years = [1990, 1967, 2002, 2010, 2018];
// console.log(calcAge(years[0]));
// console.log(calcAge(years[1]));
// console.log(calcAge(years[2]));
// console.log(calcAge(years[3]));
// console.log(calcAge(years[4]));

// const friends = ['Mike', 'Steven', 'Peter'];
// const firstlen = friends;
// console.log('firstlen', firstlen);
// const newLenght = friends.push('Alex');
// console.log(friends);
// console.log(newLenght);
// friends.unshift('John'); // insert the first item
// console.log(friends);
// friends.pop(); // remove the last item
// const popped = friends.pop();
// console.log(popped); // Peter
// console.log(friends);
// friends.shift(); // remove the first item
// console.log(friends);
// console.log(friends.indexOf('Steven')); // return index of the item if it exists
// friends.push(23)
// console.log(friends.indexOf('Bob')); // return -1 if it does not exist
// console.log(friends.includes('Steven')); // return True if item exists
// console.log(friends.includes('Bob')); // return False if item does not exist
// console.log(friends.includes('23')); // return False if item does not exist

// const bills = [125, 555, 44];

// const tips = [];
// const calcTip = function (bill) {
//     if (bill >= 50 && bill <= 300) {
//         tips.push(bill * 0.15);
//     } else {
//         tips.push(bill * 0.2);
//     }
// }

// const calcTip = function (bill) {
//     return bill >= 50 && bill <= 300 ? bill * 0.15 : bill * 0.2;
// }

// tips.push(calcTip(bills[0]), calcTip(bills[1]), calcTip(bills[2]));
// console.log(bills, tips);
// const total = [];
// total.push(bills[0] + tips[0], bills[1] + tips[1], bills[2] + tips[2]);
// console.log(total);

// const jonas = {
//     firstname: 'Jonas',
//     lastname: 'Zubchyk',
//     age: 2037 - 1991,
//     job: 'teacher',
//     friends: ['Michael', 'Peter', 'Steven'],
// }

// console.log(jonas);
// console.log(jonas.firstname);
// console.log(jonas.lastname);
// console.log(jonas['lastname']);

// const interestedIn = prompt('What do you want to know about me?');

// if (jonas[interestedIn]) {
//     console.log(jonas[interestedIn]);
// } else {
//     console.log('Wrong request!');
// }

// jonas.location = 'Portugal';
// jonas['twitter'] = '@jonas';
// console.log(jonas);
// console.log(`${jonas.firstname} has ${jonas.friends.length} friends, and his best friend is called ${jonas.friends[0]}`);

// const jonas = {
//     firstname: 'Jonas',
//     lastname: 'Zubchyk',
//     birthday: 1991,
//     job: 'teacher',
//     friends: ['Michael', 'Peter', 'Steven'],
//     hasDriversLicense: true,
//     calcAge: function () {
//         console.log(this)
//         this.age = 2037 - this.birthday;
//         return this.age;
//     },
//     getSummary: function () {
//         this.sentence = `${this.firstname} is a ${this.calcAge()}-year old ${this.job}, and he has ${this.hasDriversLicense ? 'a' : 'no'} driver's license`;
//         return this.sentence;
//     },
// }
// // console.log(jonas['calcAge'](1991));
// console.log(jonas.calcAge());
// // console.log(jonas.age);
// // console.log(jonas.age);
// // console.log(jonas.age);
// console.log(jonas.getSummary());
// console.log(jonas.sentence);

// const johnSmith = {
//     fullName: 'John Smith',
//     mass: 92,
//     height: 1.95,
//     calcBMI: function () {
//         this.BMI = this.mass / this.height ** 2;
//         return this.BMI;
//     }
// }

// const markMiller = {
//     fullName: 'Mark Miller',
//     mass: 78,
//     height: 1.69,
//     calcBMI: function () {
//         this.BMI = this.mass / this.height ** 2;
//         return this.BMI;
//     }
// }

// if (johnSmith.calcBMI() > markMiller.calcBMI()) {
//     console.log(`${johnSmith.fullName}'s BMI (${johnSmith.BMI}) is higher than ${markMiller.fullName}'s (${markMiller.BMI})!`);
// } else {
//     console.log(`${markMiller.fullName}'s BMI (${markMiller.BMI}) is higher than ${johnSmith.fullName}'s (${johnSmith.BMI})!`);
// }

// for (let count = 1; count <= 10; count++) {
//     console.log(`Repetition ${count}`);
// }

// const jonas = [
//     'Jonas',
//     'Zubchyk',
//     2037 - 1991,
//     'teacher',
//     ['Michael', 'Peter', 'Steven'],
//     true,
// ]

// const types = [];


// for (let i = 0; i < jonas.length; i++) {
//     console.log(jonas[i], '---->', typeof jonas[i]);
//     // filling types array
//     //types[i] = typeof jonas[i];
//     types.push(typeof jonas[i]);
// }

// console.log(types);

// const year = [1991, 2007, 1969, 2020];
// const age = []

// for (let i = 0; i < year.length; i++) {
//     age.push(2037 - year[i]);
// }

// // console.log(age);

// console.log('--- ONLY STRING ---');
// for (let i = 0; i < jonas.length; i++) {
//     if (typeof jonas[i] !== 'string') continue;
//     console.log(jonas[i], '---->', typeof jonas[i]);
// }

// console.log('--- BREAK WITH NUMBER ---');
// for (let i = 0; i < jonas.length; i++) {
//     if (typeof jonas[i] === 'number') break;
//     console.log(jonas[i], '---->', typeof jonas[i]);
// }

// const jonas = [
//     'Jonas',
//     'Zubchyk',
//     2037 - 1991,
//     'teacher',
//     ['Michael', 'Peter', 'Steven'],
//     true
// ]

// for (let i = jonas.length - 1; i >= 0; i--) {
//     console.log(i, '--->', jonas[i]);
// }

// for (let i = 1; i < 4; i++) {
//     console.log(`--- starting ex ${i}`);
//     for (let j = 1; j < 11; j++) {
//         console.log(`--- number ${j}`);
//     }
// }
// let i = 1
// while (i <= 10) {
//     console.log(`WHILE ${i}`);
//     i++;
// }
// let dice = Math.trunc(Math.random() * 6) + 1;

// while (dice !== 6) {
//     console.log(`You rolled a ${dice}`)
//     dice = Math.trunc(Math.random() * 6) + 1;
//     if (dice === 6) console.log(`Loop is about to end...`);
// }

// Chalenge

const bills = [22, 295, 176, 440, 37, 105, 10, 1100, 86, 52];
let tips = [];
let totals = [];

const calcTip = function (bill) {
    return bill >= 50 && bill <= 300 ? bill * 0.15 : bill * 0.2;
}

for (let i = 0; i < bills.length; i++) {
    tips.push(calcTip(bills[i]));
    totals.push(tips[i] + bills[i]);
}

console.log('tips->', tips);
console.log('totals->', totals);

// const calcAverage = function (arr) {
//     let sum = arr.reduce((acc, curr) => acc + curr, 0);
//     return sum / arr.length;
// }

const calcAverage = function (arr) {
    let sum = 0;
    for (let i = 0; i < arr.length; i++) {
        sum += arr[i];
    }
    return sum / arr.length;
}

console.log('calcAverage->', calcAverage(totals));

